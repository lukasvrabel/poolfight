using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AIPlayer : Player {
	// duration of the move in seconds
	public float maxMoveTime = 2f;
	
	protected float startMoveTime;
	
	protected CamControl camControl;
	protected GameObject activeBall;
	protected GameObject targetBall;
	
	protected Vector3 forceDirection;
	protected Vector3 actForce;
	
	
	public float randomForceStrength;
	public float randomForceAngle;

	
	public AIPlayer(string newname, CircularList<GameObject> newballs) : base(newname, newballs) {
		camControl = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<CamControl>();
		randomForceStrength = 0.1f;
		randomForceAngle = 2f;
	}
	
	
	public override void StartShooting(GameObject ball) {
		Ray ray = new Ray();
		float maxForce = 1f;
		float newzoom;
		
		
		
		//forceControl.SelectBall(ball);
		activeBall = ball;
		targetBall = null;
		ray.origin = ball.transform.position;
		
		
		// find closes available ball
		targetBall = GetNearestEnemyBall(activeBall, true);
		
		if(targetBall == null) {
			targetBall = GetNearestEnemyBall(activeBall, false);
			maxForce = 0.5f;
			newzoom = 1.0f;
		} else {
			maxForce = 1.0f;
			newzoom = 1.0f;
		}
		
		
		// calculate force
		ray.direction = targetBall.transform.position - ball.transform.position;
		
		// randomize strength and angle
		ray.direction = Quaternion.Euler(0, (UnityEngine.Random.value*2 - 1) * randomForceAngle, 0) * ray.direction;
		maxForce -= UnityEngine.Random.value * randomForceStrength;
		
		forceDirection = ray.direction.normalized * forceControl.maxForce * maxForce;	
		camControl.RotateTo(forceDirection, newzoom);
		
		// start moving
		startMoveTime = Time.time;
		actForce = Vector3.zero;
		
		forceControl.lr.SetPosition(0, ball.transform.position);
		forceControl.lr.SetPosition(1, ball.transform.position);
		forceControl.lr.enabled = true;
		
		
	}
	
	public override bool Shooting() {
		//if(activeBall == null) return false;
		
		// wait for some time, aim, and then shoot
		if(Time.time < startMoveTime + maxMoveTime) {
			// pull the force indicator
			actForce = Vector3.Lerp(actForce, forceDirection, 0.02f);
			forceControl.lr.SetPosition(1, activeBall.transform.position-actForce);
			return true;
		} else {
			// save for replay and shoot 
			shotHistory.Enqueue(forceDirection);
			activeBall.rigidbody.AddForce(forceDirection * forceControl.force);
			forceControl.lr.enabled = false;
			
			return false;
		}
		
		
	}
	
	
	
	
		
//	public Player(string _name) {
//		Player(_name, new List<GameObject>());
//	}
//	
//	public Player(List<GameObject> _balls) {
//		_newPlayerID ++;
//		Player("Player " + _newPlayerID, _balls);
//	}
//	 
//	public Player() {
//		Player(new List<GameObject>());
//	}
	

}
