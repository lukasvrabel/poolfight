using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]

public class AudioHits : MonoBehaviour {
	
	public AudioClip tablehit;
	public AudioClip plastichit;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void OnCollisionEnter(Collision collision) {
		
		if (collision.relativeVelocity.magnitude > 0.5f) {
			if(collision.gameObject.GetComponent<MaterialSound>()) {
				audio.clip = collision.gameObject.GetComponent<MaterialSound>().clip;
			} else {
			}
				
	//			if(collision.gameObject.tag == Tags.Ball) {
	//				audio.clip = plastichit;
	//			} else if (collision.gameObject.tag == Tags.GamePlane) {
	//				audio.clip = tablehit;
	//			} else if (collision.gameObject.tag == Tags.Box) {
	//				audio.clip = tablehit;
	//			} else {
	//				audio.clip = tablehit;
	//			}
				
			audio.volume = (collision.relativeVelocity.magnitude / 5 + 0.9f);
			audio.pitch = (Random.value * 0.5f + 0.5f);
			audio.Play();
			
			//AudioSource.PlayClipAtPoint(tablehit, collision.transform.position);
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
