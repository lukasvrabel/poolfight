using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReplayPlayer : AIPlayer {

	public Queue<Vector3> replayShots;
	
	public ReplayPlayer(string newname, CircularList<GameObject> newballs, Queue<Vector3> replay) : base(newname, newballs) {
		replayShots = replay;
	}
	
	public override void StartShooting(GameObject ball) {
		activeBall = ball;
		
		startMoveTime = Time.time;
		actForce = Vector3.zero;
		
		forceControl.lr.SetPosition(0, ball.transform.position);
		forceControl.lr.SetPosition(1, ball.transform.position);
		forceControl.lr.enabled = true;
		
		if(replayShots.Count > 0) {
			forceDirection = replayShots.Dequeue();
		} else {
			forceDirection = Vector3.zero;
		}
		
	}
}
