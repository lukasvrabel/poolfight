using UnityEngine;
using System;
using System.Collections;

public class GUIController : MonoBehaviour {
	public enum StripColor {Orange, Blue, Red}
	public enum Menu {Nothing, Level1, Level2, Level3, PlayComputer, PlayPlayer, Resume, Restart, MainMenu, Quit}
	
	public Texture orangeStrip;
	public Texture redStrip;
	public Texture blueStrip;
	
	public Font labelFont;
	public int fontSizeRatio = 16;
	private GUIStyle labelStyle;
	
	
	private float movingX = Mathf.Infinity;
	private float actualY;
	
	private Texture actualStrip;
	private string actualText;
	
	private Texture2D fadeTexture;
	private Color fadeColor;

	
	
	// Use this for initialization
	void Start () {
		//tSD = (Texture)Resources.Load("Textures/SD.png");
		labelFont = (Font)Resources.Load("verdana.ttf");
		
		
		labelStyle = new GUIStyle();
		labelStyle.font = labelFont;
		labelStyle.fontSize = Screen.height/fontSizeRatio;// 55;
		labelStyle.fontStyle = FontStyle.BoldAndItalic;
		labelStyle.normal.textColor = Color.white;
		
		Reset();
		
		fadeColor = Color.black;
		fadeTexture = new Texture2D(1, 1);
		fadeTexture.SetPixel(0, 0, fadeColor);
		fadeTexture.Apply();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		// debug options
		if(Input.GetKeyDown(KeyCode.A)) {
			StripAcross(StripColor.Orange, "SUDDEN DEATH");
		}
		if(Input.GetKeyDown(KeyCode.S)) {
			StripIn(StripColor.Blue, "BLUE WINS");
		}
		if(Input.GetKeyDown(KeyCode.D)) {
			StripIn(StripColor.Red, "RED WINS");
		}
		if(Input.GetKeyDown(KeyCode.F)) {
			StripIn(StripColor.Orange, "DRAW");
			
		}
		if(Input.GetKeyDown(KeyCode.G)) {
			StripOut();
		}
		if(Input.GetKeyDown(KeyCode.Q)) {
			FadeToBlack(0.3f);
		}
		if(Input.GetKeyDown(KeyCode.W)) {
			FadeToClear(0.3f);
		}
		
	}
	
	void OnGUI() {
		if(actualStrip != null) {
			DrawStripLabel(actualText, actualStrip, movingX, actualY);
		}
		
		if(fadeColor.a > 0) {
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
		}
		
		return;
		
			
	}
	
	
	// UTILITY FUNCTIONS
	
	public void Reset() {
		//StripOut();
	}

	
	
	// MENU DRAWING FUNCTIONS
	
	
	public Menu MainMenu() {
		Rect menuBox = CenterRect(Screen.width/2, Screen.height/2, 500, 350);
		GUI.Box(menuBox, "Main Menu");
		
		// Level 1
		if (GUI.Button(CenterRect(menuBox.x + 100, menuBox.y + 100, 100, 100), "Level 1")) {
			return Menu.Level1;
		}
		
		// Level 2
		if (GUI.Button(CenterRect(menuBox.x + 250, menuBox.y + 100, 100, 100), "Level 2")) {
			return Menu.Level2;
		}
		
		// Level 3
		if (GUI.Button(CenterRect(menuBox.x + 400, menuBox.y + 100, 100, 100), "Level 3")) {
			return Menu.Level3;
		}
		
		// vs computer
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 200, 200, 30), "Play vs Computer")) {
			return Menu.PlayComputer;
		}
		
		// vs player
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 250, 200, 30), "Play vs Player")) {
			return Menu.PlayPlayer;
		}
		
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 300, 200, 30), "Quit")) {
			return Menu.Quit;
		}
		
		return Menu.Nothing;
	}
	
	public Menu GameMenu() {
		Rect menuBox = CenterRect(Screen.width/2, Screen.height/2 + 100, 300, 250);
		GUI.Box(menuBox, "Game Menu");
		
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 50, 200, 30), "Resume")) {
			return Menu.Resume;
		}
		
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 100, 200, 30), "Restart")) {
			return Menu.Restart;
		}
		
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 150, 200, 30), "Back to Main Menu")) {
			return Menu.MainMenu;
		}
		
		if (GUI.Button(CenterRect(Screen.width/2, menuBox.y + 200, 200, 30), "Quit")) {
			return Menu.Quit;
		}
		
		return Menu.Nothing;
	}
	
	
	
	private IEnumerator FadeTexture(Color fromColor, Color toColor, float duration) {
		float startTime = Time.time;
		float t;
		
		while(Time.time - startTime <= duration*1.1) {
			t = (Time.time - startTime) / duration;
			fadeColor = Color.Lerp(fromColor, toColor, t);
			fadeTexture.SetPixel(0, 0, fadeColor);
			fadeTexture.Apply();
		
			yield return 0;
		}
		fadeColor = toColor;
		
	}
	
	public void FadeToBlack(float duration) {
		StartCoroutine(FadeTexture(fadeColor, Color.black, duration));
	}
	
	public void FadeToClear(float duration) {
		StartCoroutine(FadeTexture(fadeColor, Color.clear, duration));
	}
	
	
	// STRIP DRAWING FUNCTIONS
	// draw strip label at set position
	private void DrawStripLabel(string text, Texture strip, float centerX, float centerY) {
		// resize strip and label to screen
		float stripWidth = Screen.width*2f;
		float stripHeight = strip.height * (stripWidth/strip.width);
		labelStyle.fontSize = Screen.height/fontSizeRatio;
		Vector2 labelSize = labelStyle.CalcSize(new GUIContent(text));
		
		
		GUI.DrawTexture(CenterRect(centerX, centerY, stripWidth, stripHeight), strip);
		GUI.Label(CenterRect(centerX, centerY, labelSize.x, labelSize.y), text, labelStyle);
		
		//DrawStrip(strip, centerX, centerY);
		//DrawLabel(text, centerX, centerY);
	}
	
	// set up moving color strip label with text mowing from min to max at centerY in duration
	private void StripLabel(StripColor color, string text, float duration, float centerY, float min, float max, Func<float, float> func) {
		StartCoroutine(MoveX(min, max, duration, func));
		actualText = text;
		actualY = centerY;
		
		switch(color) {
		case StripColor.Blue:
			actualStrip = blueStrip;
			break;
			
		case StripColor.Red:
			actualStrip = redStrip;
			break;
			
		case StripColor.Orange:
			actualStrip = orangeStrip;	
			break;
			
		default:
			actualStrip = orangeStrip;
			break;
		}
	}
	
	public void StripAcross(StripColor color, string text, float duration, float centerY) {
		StripLabel(color, text, duration, centerY, -Screen.width, 2 * Screen.width, Sigmoid);
	}
	public void StripAcross(StripColor color, string text, float duration = 2f) {
		StripAcross(color, text, duration, Screen.height * 0.7f);
	}
	
	public void StripIn(StripColor color, string text, float duration, float centerY) {
		StripLabel(color, text, duration, centerY, -Screen.width, 0.5f * Screen.width, SlowDown);
	}
	public void StripIn(StripColor color, string text, float duration = 1f) {
		StripIn(color, text, duration, Screen.height * 0.3f);
	}
	
	public void StripOut(float duration = 0.5f) {
		StartCoroutine(MoveX(movingX, 2 * Screen.width, duration, SpeedUp));
	}
	
	

	// MOVING FUNCTIONS
	
	IEnumerator MoveX(float min, float max, float duration, Func<float, float> func) {
		float startTime = Time.time;
		float t;
		
		//if(min > max) return false;
		
		// add 1% to duration, so it is easier to make the last step
		while(Time.time - startTime <= duration*1.1) {
			t = (Time.time - startTime) / duration;
			movingX = Mathf.Lerp(min, max, func(t));
			yield return 0;
		}
		
	}
	
	
	private float Sigmoid(float x) {
		float ret = 15f * Mathf.Pow( x-0.5f, 5 ) + 0.1f * x + 0.48f;
		//Debug.Log(ret);
		return Mathf.Clamp(ret, 0f, 1f);
		
	}
	private float Linear(float x) {
		return Mathf.Clamp(x, 0f, 1f);
	}
	private float SlowDown(float x) {
		//float ret = Mathf.Sin (x * Mathf.PI * 0.5f);
		//float ret = Mathf.Log(x+0.01f)/4f + 1f;
		float ret = Mathf.Pow (x-1f, 3f) + 1f; 
		return Mathf.Clamp(ret, 0f, 1f);
	}
	private float SpeedUp(float x) {
		//float ret = 1.0f - Mathf.Cos (x * Mathf.PI * 0.5f);
		float ret = Mathf.Pow(x, 3);
		return Mathf.Clamp(ret, 0f, 1f);
	}
	
	
	
	
	// drawing functions
	
	
	/// <summary>
	/// Create rect with center on (centerX, centerY). If centerX or centerY is less than zero, it will center on screen.
	/// </summary>
	/// <returns>
	/// Centered rect.
	/// </returns>
	/// <param name='width'>
	/// Width of rect.
	/// </param>
	/// <param name='height'>
	/// Height of rect.
	/// </param>
	/// <param name='centerX'>
	/// Value of X-coordinate of center of the rect. If less than zero, then the rect will be centered on screen for this coordinate.
	/// </param>
	/// <param name='centerY'>
	/// Value of Y-coordinate of center of the rect. If less than zero, then the rect will be centered on screen for this coordinate.
	/// </param>
	Rect CenterRect(float centerX, float centerY, float width, float height) {
		centerX -= width/2;
		centerY -= height/2;	
		return new Rect(centerX, centerY, width, height);
		
	}
	


	
}
