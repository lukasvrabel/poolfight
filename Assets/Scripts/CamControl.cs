using UnityEngine;
using System;
using System.Collections;

public class CamControl : MonoBehaviour {
	
	enum States {Illegal, Idle, Centering, Rotozooming, Rotating, RotateTo, Track, LookAt, Overview};
	
    public float zoomSpeed = 0.6f;
    public float rotationSpeed = 100.0f;

	
	public float minZoom = 0f;
	public float maxZoom = 1f;
	
	public bool rotationEnabled = true;
	public bool locked; 
	
	public Camera cam;
	private States state;
	
	// center we are looking to
	private Vector3 center;
	private float zoom;
	private float angle;
	private GameObject trackedObject;
	private float camSpeed;
	
	private Vector3 lookAt;
	
	
	
	private Vector3 zoomedOutOffset;
	private Vector3 zoomedInOffset;
	
	private Vector3 destPosition;
	private Quaternion destRotation;
	
	
	public void Reset() {
		center = Vector3.zero;
		zoom = 0.5f;
		angle = 0f;
		trackedObject = null;
		camSpeed = 10f;
		
		SetDest(center, angle, zoom);
		locked = false;
		state = States.Idle;
	}
	
	void Start() {
		state = States.Idle;
		//OnLevelWasLoaded(0);
		//Reset ();	
	}
	
	public void OnLevelWasLoaded(int level) {
		cam = Camera.mainCamera;
		zoomedOutOffset = GameObject.FindGameObjectWithTag(Tags.UnzoomedCamera).transform.position;
		zoomedInOffset = GameObject.FindGameObjectWithTag(Tags.ZoomedCamera).transform.position;
//		Debug.Log("CAM LEVEL LOAD:" + zoomedInOffset + " " + zoomedOutOffset);
		Reset();
	}
	
    void Update() {
		if(cam == null) return;
		
		cam.transform.position = Vector3.Lerp(cam.transform.position, destPosition, camSpeed * Time.deltaTime);
		cam.transform.rotation = Quaternion.Lerp(cam.transform.rotation, destRotation, camSpeed * Time.deltaTime);
		
		switch(state) {
			
		case States.Idle:
			// waiting for player input
			
			// Fire2 is rotozooming
			if(rotationEnabled && Input.GetButtonDown ("Fire2")) {
				ChangeState (States.Rotozooming, 10f);
				
			// Tab is overview
			} else if (Input.GetButtonDown("Overview")) {
				destPosition = new Vector3(0f, 10, 0f);
				destRotation = Quaternion.LookRotation(Vector3.down);
				ChangeState (States.Overview, 10f);
			}
			
			break;
			
		case States.Overview:
			// look at table from above
			
			if (Input.GetButtonDown("Overview")) {
				// get back to center/angle/zoom position
				SetDest(center, angle, zoom);
				ChangeState(States.Idle, 10f);
			}
			break;
			
		case States.Rotozooming:
			// check for end rotozooming
			if( Input.GetButtonUp("Fire2") ) {
				ChangeState (States.Idle);
				state = States.Idle;
				break;
			}
			
			// get input for rotozooming
		    float zoomdelta = Input.GetAxis("Mouse Y") * zoomSpeed;
		    float rotation = Input.GetAxis("Mouse X") * rotationSpeed;
		    zoomdelta *= Time.deltaTime;
		    rotation *= Time.deltaTime;
			
			// rotate and zoom
			zoom = Mathf.Clamp(zoom + zoomdelta, minZoom, maxZoom);
			angle = Mathf.Repeat(angle+rotation, 360f);
			SetDest(center, angle, zoom);
			
			break;
			
		case States.Centering:
			// move and rotate camera to destination
			
			// check if we are close enough
			Vector3 dist = cam.transform.position - destPosition;
			if (dist.magnitude < 0.1f){
				ChangeState (States.Idle);			
			}
			
			break;
			
		case States.Rotating:
			angle += 20f * Time.deltaTime;
			SetDest(center, angle, zoom);
			break;
			
		case States.Track:
			if(trackedObject == null) {
				ChangeState(States.Idle);
				break;
			}
			destRotation = Quaternion.LookRotation(trackedObject.transform.position - cam.transform.position);
			
			break;
			
		case States.LookAt:
			destRotation = Quaternion.LookRotation(lookAt - cam.transform.position);
			break;
			
		case States.RotateTo:
			
			break;
			
		default:
			throw new Exception(String.Format("Illegal camera state <{0}>", state));
		}
		
		
	
			
			
		
    }
	
	
	public void MoveToObject(GameObject o) {
		if(locked) return;
		center = o.transform.position;
		
		// we dont need to do stuff in overview
		if(state == States.Overview) return;
		
		SetDest(center, angle, zoom);
		ChangeState(States.Centering, 3f);
	}
	
	
	public void Track(GameObject o) {
		if(locked) return;
		trackedObject = o;
		
		// we dont need to do stuff in overview
		if(state == States.Overview) return;
		
		ChangeState(States.Track, 1f);
	}
	
	public void TrackBalls(CircularList<GameObject> balls) {
		if(locked) return;
		// we dont need to do stuff in overview
		if(state == States.Overview) return;
		
		
		Vector3 center = Vector3.zero;
		balls.ForEach( x => center += x.transform.position );
		center /= balls.Count;
		
		foreach(GameObject b in balls) {
			Debug.DrawLine(b.transform.position, b.transform.position + Vector3.up, Color.green);
		}
		Debug.DrawLine(center, center + Vector3.up, Color.red);
		
		lookAt = center;
		
		ChangeState(States.LookAt, 1f);
	}
	
	public void RotateAround(GameObject o, float newzoom, float speed) {
		if(locked) return;
		zoom = newzoom;
		center = o.transform.position;
		SetDest(center, angle, zoom);
		ChangeState(States.Rotating, speed);
	}
	
	public void RotateAround(GameObject o, float newzoom) {
		RotateAround(o, newzoom, 1f);
	}
	
	public void RotateAround(GameObject o) {
		RotateAround(o, zoom, 1f);
	}
	
	
	public void ZoomOut() {
		if(locked) return;
		zoom = 0.0f;
		
		// we dont need to do stuff in overview
		if(state == States.Overview) return;
				
		SetDest(center, angle, zoom);
		ChangeState(States.Centering, 3f);
	}
	
	

	
	public void RotateTo(Vector3 direction, float newzoom) {
		if(locked) return;
		float newangle = Vector3.Angle(Vector3.forward, direction);
		
		// calculate dot product for determinig the side of angle
		Vector3 perp = Vector3.Cross(Vector3.forward, direction);
		float dot = Vector3.Dot(perp, Vector3.up);
		if(dot < 0) {
			newangle = 360-newangle;
		}

		
		zoom = newzoom;
		angle = newangle;
		
		// we dont need to do stuff in overview
		if(state == States.Overview) return;
		
		SetDest (center, newangle, newzoom);
		ChangeState(States.Idle, 3f);
	}
	
	public void RotateTo(GameObject o) {
		if(o == null) return;
		Vector3 direction = o.transform.position - center;
		RotateTo(direction);	
	}
	
	public void RotateTo(Vector3 direction) {
		RotateTo(direction, zoom);
	}
	
	
	
	
	
	// calculate position from center, angle and zoom
	
	private void GetPosition(Vector3 center, float angle, float zoom, out Vector3 position, out Quaternion rotation) {
		// lerp to offset between zoomed in and zoomed out
		Vector3 offset = Vector3.Lerp(zoomedOutOffset, zoomedInOffset, zoom);
		
		// add rotated offset to center
		position = center + Quaternion.Euler(0, angle, 0) * offset;
		
		// look at center
		rotation = Quaternion.LookRotation(center-position);
	}
	
	private void GetPosition(out Vector3 position, out Quaternion rotation) {
		GetPosition(center, angle, zoom, out position, out rotation);
	}
	
	private void SetDest(Vector3 center, float angle, float zoom) {
		GetPosition(center, angle, zoom, out destPosition, out destRotation);
	}
	
	private void SetDest() {
		SetDest(center, angle, zoom);
	}
	
	
	
	private void ChangeState(States newState, float cameraSpeed) {
		if(locked) return;
		state = newState;
		camSpeed = cameraSpeed;
	}
	
	private void ChangeState(States newState) {
		ChangeState(newState, camSpeed);
	}
	
}
