using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	enum States {Illegal, Nothing, Init, InitMove, Shooting, waitBallsActive, RemoveBalls, SDInit, SDLowerObstacles, SDInitMove, Final}
	
	// player list
	public CircularList<Player> players;
	
	// prefab for balls
	//public GameObject blueBall;
	//public GameObject redBall;
	
	// this object hierarchy will lower in sudden death mode
	public GameObject lowerPlatform;
	const float lowerMinY = -0.45f;
	const float lowerSpeed = 0.05f;
	
	
	
	
	//public bool playDemo = true;
	
	// camera control script
	private CamControl camControl;
	private ForceControl forceControl;
	private GUIController guiControl;
	
	// game plane
	private GameObject gamePlane;
	
	// list of all active balls
	private CircularList<GameObject> allBalls;
	// list of all balls to be removed;
	private CircularList<GameObject> toRemove;
	
	// state of main FSM
	private States state = States.Illegal;
	//private bool physicsActive;
	
	// minimum/maximum time between turn switch
	public float minPhysicsTime = 1f;
	public float maxPhysicsTime = 12f;
	
	// active player and active ball
	private Player activePlayer;
	private GameObject activeBall;
	
	private bool demo;
	private bool debug;
	
	
	public Texture texRedBall;
	public Texture texBlueBall;
	public GameObject prefabBall;
	
	
	public void Reset() {
		lowerPlatform.transform.position = Vector3.zero;
		guiControl.Reset();
		ChangeState(States.Nothing);
		
	}
	
	
	void Start() {
		guiControl = GameObject.FindGameObjectWithTag(Tags.MainController).GetComponent<GUIController>();
		//camControl = GameObject.FindGameObjectWithTag(Tags.MainController).GetComponent<CamControl>();
		//forceControl = GameObject.FindGameObjectWithTag(Tags.MainController).GetComponent<ForceControl>();
		
		debug = false;
	}
	
	public void OnLevelWasLoaded(int level) {
		gamePlane = GameObject.FindGameObjectWithTag(Tags.GamePlane);
		lowerPlatform = GameObject.FindGameObjectWithTag(Tags.LowerPlatform);
		
		camControl = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<CamControl>();
		forceControl = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<ForceControl>();
		
		camControl.OnLevelWasLoaded(level);
		
		forceControl.Reset();
		
		//DestroyPlayers(players);
		//Reset();
	}
	
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.X)) {
			debug = !debug;
		}
		if(Input.GetKeyDown(KeyCode.F1)) {
			OnLevelWasLoaded(1); 
			NewGame(false, true);
		}
		
	}
	
	/// <summary>
	/// Control logic behind normal game phase.
	/// </summary>
	IEnumerator NormalUpdate(CircularList<GameObject> allBalls, CircularList<Player> players) {
		Player activePlayer;
		
		List<Player> replayPlayers = new List<Player>(players);
		
		// center view to game plane and wait for balls to calm down
		camControl.MoveToObject(gamePlane);
		yield return StartCoroutine( WaitAndRemove(allBalls) );
		

		while( ! IsEnd(players) ) {
			// get next player and next ball
			players.MoveNextCircular();
			activePlayer = players.Current;
			activePlayer.balls.MoveNextCircular();
			//activeBall = activePlayer.balls.Current;
			
			// shoot 
			yield return StartCoroutine( ActivateAndShoot(activePlayer) );
			
			// wait until physics activate and then track fastest ball
			yield return new WaitForFixedUpdate();
			//StartCoroutine(TrackFastest(allBalls));
			StartCoroutine(TrackMovingBalls(allBalls));
			
			// wait until balls calm down, then remove deactivated balls
			yield return StartCoroutine( WaitAndRemove(allBalls) );
			
			if(IsEnd(players)) break;
			
			// if there is only one ball left, then enter suddendeath
			foreach(Player p in players) {
				if(p.balls.Count <= 1) {
					// start sudden death update, it will return at end game
					yield return StartCoroutine( SDUpdate(allBalls, players) );
					break;
				}
			}
			
				
		}
		
		// end
		yield return StartCoroutine( EndGame(replayPlayers) );
		
		
	}
	
	/// <summary>
	/// Control logic for sudden death phase of game
	/// </summary>
	/// <returns>
	/// Returns when the end game condition is satisfied
	/// </returns>
	IEnumerator SDUpdate(CircularList<GameObject> allBalls, CircularList<Player> players) {
		Player activePlayer;
		
		// show label
		guiControl.StripAcross(GUIController.StripColor.Orange, "SUDDEN DEATH", 2f);
		yield return new WaitForSeconds(2f);
		
		// reset balls index
		foreach(Player p in players) {
			p.balls.Reset();
		}
		
		// get next player
		players.MoveNextCircular();
		activePlayer = players.Current;
		
		while( ! IsEnd(players) ) {
			// if there are no more balls for active player
			//   then reset the balls and move to next player
			if(! activePlayer.balls.MoveNext() ) {
				activePlayer.balls.Reset();
				
				players.MoveNextCircular();
				activePlayer = players.Current;
				
				// lower suddent death platform
				yield return StartCoroutine( LowerPlatform(minPhysicsTime) );
				
			// else there are more balls for active player, continue playing
			} else {
			
				//activeBall = activePlayer.balls.Current;
				
				yield return StartCoroutine( ActivateAndShoot(activePlayer) );
				
				// wait until physics activate and then track fastest ball
				yield return new WaitForFixedUpdate();
				//StartCoroutine(TrackFastest(allBalls));
				StartCoroutine(TrackMovingBalls(allBalls));
				
				// wait until balls calm down, then remove deactivated balls
				yield return StartCoroutine( WaitAndRemove(allBalls) );		

				if(IsEnd(players)) break;
	
			}
		}
	}
	
	/// <summary>
	/// Activates the shooting and wait for player to shoot.
	/// </summary>
	IEnumerator ActivateAndShoot(Player activePlayer) {
		
		GameObject activeBall = activePlayer.balls.Current;
		
		// activate ball, zoom camera and select ball for force control
		camControl.MoveToObject(activeBall);
		camControl.RotateTo( activePlayer.GetNearestEnemyBall(activeBall, false) );
	
		// select ball for shooting
		activeBall.light.enabled = true;
		activePlayer.StartShooting(activeBall);
	
		// wait until player is shooting
		while(activePlayer.Shooting()) yield return 0;
		// player released force 
		
		// deselect ball
		activeBall.light.enabled = false;
		
	}
	
	/// <summary>
	/// Wait for physics and then remove inactive balls and players.
	/// </summary>
	IEnumerator WaitAndRemove(CircularList<GameObject> allBalls) {
		// wait minimum physics time
		yield return new WaitForSeconds(minPhysicsTime);
		
		// wait for physics
		float startWaitTime = Time.time;
		while ((Time.time - startWaitTime) < maxPhysicsTime && BallsActive(allBalls)) {
			// deactivate balls that are not in play area			
			CleanBalls(allBalls, toRemove);
			
			yield return new WaitForFixedUpdate();;
		}
		
		// remove balls from player lists
		foreach(Player p in players) {
			p.balls.RemoveAll(x => toRemove.Contains(x));
		}
		
		toRemove.Clear();
		
		// remove all player without balls
		players.RemoveAll(x => ! x.HasBalls());
	}
	
	/// <summary>
	/// Lowers the platform for sudden death.
	/// </summary>
	IEnumerator LowerPlatform(float loweringTime) {
		float startWaitTime = Time.time;
		
		camControl.Track(gamePlane);
		// slowly lower obstacles for loweringTime duration
		while((Time.time - startWaitTime < loweringTime) && lowerPlatform.transform.position.y > lowerMinY) {
			lowerPlatform.transform.Translate(0f, -lowerSpeed * Time.deltaTime, 0f);	
			yield return 0;
		} 
	}

	/// <summary>
	/// Raises the platform back to zero.
	/// </summary>
	IEnumerator RaisePlatform() {
		while(lowerPlatform.transform.position.y < 0f) {
			lowerPlatform.transform.Translate(0f, lowerSpeed * Time.deltaTime, 0f);	
			yield return 0;
		} 
	}
	
	
	/// <summary>
	/// Tracks the fastest ball from allBalls
	/// </summary>
	IEnumerator TrackFastest(CircularList<GameObject> allBalls) {
		while(BallsActive(allBalls)) {

			// find fastest ball and focus camera
			camControl.Track(
			 	allBalls.FindMax(
					(GameObject x, GameObject y) => x.rigidbody.velocity.magnitude > y.rigidbody.velocity.magnitude
				)
			);	
			yield return 0;
		}
	}
	
	/// <summary>
	/// Tracks moving Balls.
	/// </summary>
	IEnumerator TrackMovingBalls(CircularList<GameObject> allBalls) {
		CircularList<GameObject> trackedBalls = new CircularList<GameObject>();
		
		while(BallsActive(allBalls)) {
			foreach(GameObject ball in allBalls) {
				if(ball.rigidbody.velocity.magnitude > 0.1f && !trackedBalls.Contains(ball)) {
					trackedBalls.Add(ball);
				}
			}
			
			camControl.TrackBalls(trackedBalls);
			
			yield return 0;
		}
	}
	
	/// <summary>
	/// Process the end game stuff (draw label, raise platform, reset demo)
	/// </summary>
	IEnumerator EndGame(List<Player> replayPlayers) {
		camControl.RotateAround(gamePlane, 0.1f);
		
		// draw endgame label
		if(players.MoveNextCircular()) {
			if(players.Current.name == "Blue") {
				guiControl.StripIn(GUIController.StripColor.Blue, "BLUE WINS");
			} else {
				guiControl.StripIn(GUIController.StripColor.Red, "RED WINS");
			}
		} else {
			guiControl.StripIn(GUIController.StripColor.Orange, "DRAW");
		}
		
		StartCoroutine( RaisePlatform() );
		

		if(demo) {
			// reset gui and wait 1 sec for its cleanup
			yield return new WaitForSeconds(4);
			guiControl.StripOut(0.3f);
			guiControl.FadeToBlack(0.3f);
			yield return new WaitForSeconds(0.3f);

			Reset();
			Demo();
			
			guiControl.FadeToClear(0.3f);
			
		} else {
			//Replay(replayPlayers);
		}
	}
	
	
	public void NewGame(bool p1AI, bool p2AI) {
		demo = false;		
		forceControl.Reset();
		camControl.locked = false;
		
		// create new players with balls
		DestroyPlayers(players);
		players = CreatePlayers(p1AI, p2AI);
		
		
		allBalls = new CircularList<GameObject>();
		toRemove = new CircularList<GameObject>();
		
		// add balls to allBalls
		players.ForEach( x => allBalls.AddRange(x.balls) );
		
		Reset();
		
		// start game logic
		StartCoroutine(NormalUpdate(allBalls, players));
		
	}
	
	public void Demo () {
		
		NewGame(true, true);
		
		camControl.RotateAround(gamePlane, 0.0f);
		camControl.locked = true;
		demo = true;
	
	}
	
	public void Replay (List<Player> replayPlayers) {		
		// TODO gui draw camera skin
		
		forceControl.Reset();
		
		// create new players with balls
		DestroyPlayers(players);
		players = CreatePlayers(true, true, replayPlayers);
		
		
		allBalls = new CircularList<UnityEngine.GameObject>();
		toRemove = new CircularList<GameObject>();
		
		// add balls to allBalls
		players.ForEach( x => allBalls.AddRange(x.balls) );
		
		//Reset();
		
		
		camControl.RotateAround(gamePlane, 0.0f);
		camControl.locked = true;
		demo = true;

		StartCoroutine(NormalUpdate(allBalls, players));
	}
	
	
	
	
	void OnGUI() {
		// TODO
		if(debug) {
			GUI.Label(new Rect(0, 0, 500, 20), "State: " + state);
			GUI.Label(new Rect(0, 20, 500, 20), "Physics: " + BallsActive (allBalls));
			GUI.Label(new Rect(0, 40, 500, 20), "demo: " + demo);
			GUI.Label(new Rect(0, 60, 500, 20), "cam lock: " + camControl.locked);
			
		}
		
	}
	
	
	void DestroyPlayers(CircularList<Player> players) {
		if(players == null) return;
		// destroy balls
		foreach(Player p in players) {
			p.balls.ForEach(x => Destroy(x));
		}

	}
	
	CircularList<Player> CreatePlayers(bool p1AI, bool p2AI, List<Player> replayPlayers = null) {
		CircularList<GameObject> blueBalls;
		CircularList<GameObject> redBalls;
		CircularList<Player> players;
		GameObject[] spawns;
		
		// create players and balls
		players = new CircularList<Player>();
		
		
		////////////////
		// Red player //
		////////////////
		
		// get spawn points and sort them by name
		spawns = GameObject.FindGameObjectsWithTag(Tags.SpawnPointRed);
		Array.Sort(spawns, (GameObject x, GameObject y) => x.name.CompareTo(y.name));
		
		// create ball on each spawn point
		redBalls = new CircularList<GameObject>();
		foreach(GameObject spawn in spawns) {
			GameObject ball = Instantiate(prefabBall, spawn.transform.position, Quaternion.identity) as GameObject;
			ball.GetComponent<MeshRenderer>().material.mainTexture = texRedBall;
			redBalls.Add(ball);
		}

		// create player
		if(replayPlayers != null) {
			players.Add(new ReplayPlayer("Red", redBalls, replayPlayers[0].shotHistory));
		} else if(p1AI) {
			players.Add(new AIPlayer("Red", redBalls));
		} else {
			players.Add(new Player("Red", redBalls));
		}

		
		
		
		/////////////////
		// Blue player //
		/////////////////
		// get spawn points and sort them by name
		spawns = GameObject.FindGameObjectsWithTag(Tags.SpawnPointBlue);
		Array.Sort(spawns, (GameObject x, GameObject y) => x.name.CompareTo(y.name));
		
		// create ball on each spawn point
		blueBalls = new CircularList<GameObject>();
		foreach(GameObject spawn in spawns) {
			GameObject ball = Instantiate(prefabBall, spawn.transform.position, Quaternion.identity) as GameObject;
			ball.GetComponent<MeshRenderer>().material.mainTexture = texBlueBall;
			blueBalls.Add(ball);
		}
		
		// create player
		if(replayPlayers != null) {
			players.Add(new ReplayPlayer("Blue", blueBalls, replayPlayers[1].shotHistory));
		} else if(p2AI) {
			players.Add(new AIPlayer("Blue", blueBalls));
		} else {
			players.Add(new Player("Blue", blueBalls));
		}
			
		// after the players are created, set enemy balls for each player
		players[0].SetEnemyBalls (blueBalls);
		players[1].SetEnemyBalls (redBalls);
		
		return players;
	}
	
	
	
	bool IsEnd(CircularList<Player> players) {
		if(players.Count <= 1)
			return true;
		
		return false;
	}
	
	bool BallsActive (List<GameObject> balls) {
		bool active = false;
		foreach(GameObject ball in balls) {
			if(ball.rigidbody.velocity.magnitude > 0.01f) {
			//if( !ball.rigidbody.IsSleeping() ) {
				active = true;
				break;
			}
		}
		return active;
		
	}			
	
	
	
	
	// remove deactivated objects from balls, add them to 'toRemove' list
	public void CleanBalls(List<GameObject> balls, List<GameObject> toRemove) {
		foreach(GameObject ball in balls) {
			if ( ! isInPlayArea(ball) ) {
				toRemove.Add(ball);
			}
		}
		balls.RemoveAll(x => toRemove.Contains(x));
	}
	
	// check whether an object is in the playing area
	private bool isInPlayArea(GameObject ball) {
		return ball.tag != Tags.Deactivated;
	}
	
	private void ChangeState(States newState) {
		state = newState;
	}
	
}
