using UnityEngine;
using System.Collections;

public class BlobShadowRotator : MonoBehaviour {
	
	//public GameObject mainLight;
	public float offset = 1.0f;

	
	// Use this for initialization
	void Start() {
		//mainLight = GameObject.FindGameObjectWithTag(Tags.MainLight);
	}
	
	// Update is called once per frame
	void Update () {
		// get light direction
		//Vector3 lightDirection = (mainLight.transform.position - transform.parent.position);
		Vector3 lightDirection = -Vector3.down;
		
		// transform projector between parent and light
		transform.position = transform.parent.position + lightDirection.normalized * offset;
		
		
		// rotate projector away from light
		transform.rotation = Quaternion.Euler(0, transform.parent.eulerAngles.y, 0) * Quaternion.LookRotation(-lightDirection);
		

	}
}
