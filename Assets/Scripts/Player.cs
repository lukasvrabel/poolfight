using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Player {
	
	public string name;
	public CircularList<GameObject> balls;
	public CircularList<GameObject> enemyBalls;
	public Queue<Vector3> shotHistory;
	
	// user input control script for dragging of force arrow
	protected ForceControl forceControl;
	
	
	
	
	public Player(string newname, CircularList<GameObject> newballs) {
		name = newname;
		balls = newballs;
		shotHistory = new Queue<Vector3>();
		forceControl = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<ForceControl>();
	}
	
	public bool HasBalls() {
		if(balls.Count <= 0) 
			return false;
		
		return true;
	}
	
	public void SetEnemyBalls(CircularList<GameObject> balls) {
		enemyBalls = balls;
	}
	
	public GameObject GetNearestEnemyBall(GameObject activeBall, bool checkCollisions, out float distance) {
		Ray ray = new Ray();
		RaycastHit hit;
		float dist = float.PositiveInfinity;
		GameObject targetBall = null;
		
		// calculate ball radius for sphere raycast
		float ballRadius = ((SphereCollider)activeBall.collider).radius * activeBall.transform.localScale.x;
		
		// temporary disable raycast on self
		int prevLayer = activeBall.layer;
		activeBall.layer = LayerMask.NameToLayer("Ignore Raycast");			
		
		ray.origin = activeBall.transform.position;
		
		if(checkCollisions) {
			// find closest ball we can see
			foreach(GameObject ball in enemyBalls) {
				// create ray pointing to ball
				ray.direction = ball.transform.position - activeBall.transform.position;
				
				// check if we can see the ball
				if(Physics.SphereCast(ray, ballRadius, out hit)) {
					
					Debug.DrawRay(ray.origin, ray.direction, Color.red, 3);
					Debug.DrawLine(hit.transform.position, hit.transform.position+Vector3.up, Color.green, 3);
				//if(Physics.Raycast(ray, out hit)) {
					if(hit.collider == ball.collider) {
					
						// check if it is closer 
						if((activeBall.transform.position - ball.transform.position).sqrMagnitude < dist) {
							targetBall = ball;
							dist = (activeBall.transform.position - ball.transform.position).sqrMagnitude;
						}
					}		
				} else throw new Exception("Raycast hit nothing!");
				
				
			}
		} else {
			// find closest ball
			foreach(GameObject ball in enemyBalls) {
				if((activeBall.transform.position - ball.transform.position).sqrMagnitude < dist) {
					targetBall = ball;
					dist = (activeBall.transform.position - ball.transform.position).sqrMagnitude;
				}
				
			}
			
		}
		
		// restore layer
		activeBall.layer = prevLayer;
		
		distance = dist;
		return targetBall;
	}
		
	public GameObject GetNearestEnemyBall(GameObject activeBall, bool checkCollisions) {
		float tmp;
		return GetNearestEnemyBall(activeBall, checkCollisions, out tmp);	
	}
	
	
	public virtual void StartShooting(GameObject ball) {
		forceControl.Reset();
		forceControl.SelectBall(ball);
	}
	
	public virtual bool Shooting() {
		Vector3 force;
		// if player released force control, then save the input for replay
		if(forceControl.Shooting(out force) == false) {
			shotHistory.Enqueue(force);
			return false;
		} else {
			return true;
		}
	}
	
//	public Player(string _name) {
//		Player(_name, new List<GameObject>());
//	}
//	
//	public Player(List<GameObject> _balls) {
//		_newPlayerID ++;
//		Player("Player " + _newPlayerID, _balls);
//	}
//	 
//	public Player() {
//		Player(new List<GameObject>());
//	}
	

}
