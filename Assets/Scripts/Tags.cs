using UnityEngine;
using System.Collections;

public static class Tags {

	public static string GameController = "GameController";
	public static string Box = "Box";
	public static string LineRenderer = "LineRenderer";
	public static string GamePlane = "GamePlane";
	public static string Ball = "Ball";
	public static string PlayArea = "PlayArea";
	public static string Deactivated = "Deactivated";
	public static string ZoomedCamera = "ZoomedCamera";
	public static string UnzoomedCamera = "UnzoomedCamera";
	public static string SpawnPointRed = "SpawnPointRed";
	public static string SpawnPointBlue = "SpawnPointBlue";
	public static string LowerPlatform = "LowerPlatform";
	public static string MainLight = "MainLight";
	public static string MainController = "MainController";
}
