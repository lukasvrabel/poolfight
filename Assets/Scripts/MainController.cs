using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Main controller. Persistent throughout the game.
/// Contains Menus, loads levels.
/// </summary>
public class MainController : MonoBehaviour {
	enum States {Illegal, Init, MainMenu, WaitingLoad, Playing, GameMenu}
	
	public bool p1AI, p2AI;
	
	public float fadeTime = 0.3f;
	
	public CircularList<Player> players;
	
	private States state;
	
	private GameController gameController;
	private GUIController guiController;
	//private CamControl camControl;
	
	private bool loaded;
	private bool working = false;
	
	private int level = -1;
	
	
	// only one controller
	private static bool created = false;
	
	void Awake() {
		DontDestroyOnLoad(this.gameObject);
		if(created) {
			Destroy(this.gameObject);
		} else {
			created = true;
		}
		
	}
	
	// Use this for initialization
	void Start () {
		
		guiController = GameObject.FindGameObjectWithTag(Tags.MainController).GetComponent<GUIController>();
		
		
		state = States.Init;
		working = false;
	
	}
	
	public void OnLevelWasLoaded(int level) {
		gameController = GameObject.FindGameObjectWithTag(Tags.GameController).GetComponent<GameController>();
		//gameController.OnLevelWasLoaded(level);
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.isLoadingLevel) return;
		if(working) return;

		
		switch(state) {
		case States.Init:
			level = 1;
			StartCoroutine( Process(GUIController.Menu.MainMenu) );
			break;
			
		case States.Playing:
			if(Input.GetButtonDown("Menu")) {
				ChangeState(States.GameMenu);
			}
			break;
			
		case States.GameMenu:
			if(Input.GetButtonDown("Menu")) {
				ChangeState(States.Playing);
			}
			break;
			
		case States.MainMenu:
			break;
		}
	
		
	}

	
	// TODO
	void OnGUI() {
		if(Application.isLoadingLevel) return;
		
		GUIController.Menu choice = GUIController.Menu.Nothing;
		
		
		switch(state) {
		case States.MainMenu:
			choice = guiController.MainMenu();
			break;
			
		case States.GameMenu:
			choice = guiController.GameMenu();
			break;
		}
		
		if( !working && (choice != GUIController.Menu.Nothing) ) {
			StartCoroutine( Process(choice) );
		}
		
	
	}
	
	
	
	// MENU CODE
	

	
	
	
	void GoToMainMenu() {
		//StartCoroutine(RunDemo());
		ChangeState(States.MainMenu);
	}
	
	IEnumerator LoadLevel(int newLevel) {
		guiController.FadeToBlack(fadeTime);
		guiController.StripOut(fadeTime);
		yield return new WaitForSeconds(fadeTime);
		
		level = newLevel;
		Application.LoadLevel(level);
		while(Application.isLoadingLevel) yield return 0;
		
		//OnLevelWasLoaded(level);
		
		guiController.FadeToClear(fadeTime);
			
	}
	
	

	IEnumerator Process(GUIController.Menu choice) {
		working = true;
		
		
//		guiController.FadeToBlack(fadeTime);
//		guiController.StripOut(fadeTime);
//		yield return new WaitForSeconds(fadeTime);
		
		
		switch(choice){
		case GUIController.Menu.Level1:
			level = 1;
			goto case GUIController.Menu.MainMenu;
			
		case GUIController.Menu.Level2:
			level = 1;
			goto case GUIController.Menu.MainMenu;
			
		case GUIController.Menu.Level3:
			level = 1;
			goto case GUIController.Menu.MainMenu;
			
		case GUIController.Menu.PlayComputer:
			yield return StartCoroutine( LoadLevel(level) );
			p1AI = false;
			p2AI = true;
			gameController.NewGame(p1AI, p2AI);
			ChangeState(States.Playing);
			break;
			
		case GUIController.Menu.PlayPlayer:
			yield return StartCoroutine( LoadLevel(level) );
			p1AI = false;
			p2AI = false;
			gameController.NewGame(p1AI, p2AI);
			ChangeState(States.Playing);
			break;
			
		case GUIController.Menu.Quit:
			guiController.FadeToBlack(fadeTime);
			guiController.StripOut(fadeTime);
			yield return new WaitForSeconds(fadeTime);
			Application.Quit();
			break;
			
		case GUIController.Menu.Resume:
			ChangeState(States.Playing);
			break;
			
		case GUIController.Menu.Restart:
			yield return StartCoroutine( LoadLevel(level) );
			gameController.NewGame(p1AI, p2AI);
			ChangeState(States.Playing);
			break;
			
		case GUIController.Menu.MainMenu:
			
			yield return StartCoroutine( LoadLevel(level) );
			gameController.Demo();
			ChangeState(States.MainMenu);
			break;
			
		}
		
		
		yield return new WaitForSeconds(fadeTime);
		

		working = false;
		
	}
	
	
	
	
	void ChangeState(States newState) {
		state = newState;
	}
	
}
