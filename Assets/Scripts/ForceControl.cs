using UnityEngine;
using System.Collections;

public class ForceControl : MonoBehaviour {
	
	public float force = 300f;
	public float maxForce = 1f;
	public LineRenderer lr;
	
	private GameObject currentBall;
	
	private bool dragging = false;
	private Vector3 dragStart;
	private Vector3 dragEnd;
	private Plane dragPlane;
	

	public void SelectBall(GameObject ball) {
		currentBall = ball;
	}
	
	public void Reset() {
		dragging = false;
		dragStart = Vector3.zero;
		dragEnd = Vector3.zero;
		
		lr.enabled = false;
		lr.SetPosition(0, Vector3.zero);
		lr.SetPosition(1, Vector3.zero);

	}

	void Start() {
		lr = GameObject.FindWithTag(Tags.LineRenderer).GetComponent<LineRenderer>();
		
		Reset();
	}
	
	
	public bool Shooting(out Vector3 finalForce) {
		RaycastHit hit;
		Ray ray;
		float dist;
		
		//if(!selected) return;
		
		if (dragging) {
			// draw line
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (dragPlane.Raycast(ray, out dist)) {
				dragEnd = ray.GetPoint(dist);
			}
			Vector3 f = Vector3.ClampMagnitude(dragStart-dragEnd, maxForce);
			
			//lr.SetPosition(0, dragStart+f);
			lr.SetPosition(0, dragStart);
			//lr.SetPosition(1, dragStart);
			lr.SetPosition(1, dragStart-f);
			

			// fire2 - cancel drag
			if (Input.GetButtonDown("Fire2")){
				EndDrag();				
			} 
			// mouse release - apply force
			else if (Input.GetButtonUp ("Fire1")) {
				EndDrag();
				
				finalForce = f;
				
				// calculate force
				f *= force;
				currentBall.rigidbody.AddForce(f);
				
				
				return false;
			}
		
			
		}
		
		// Start of force dragging
		if (Input.GetButtonDown("Fire1")) {
        	ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        	if (currentBall.collider.Raycast(ray, out hit, Mathf.Infinity)) {
				StartDrag(currentBall.transform.position + Vector3.up*0.03f);				
				//StartDrag(transform.position);				
			}
    	}
		
		
		
		finalForce = Vector3.zero;
		return true;
		
	}
	public bool Shooting() {
		Vector3 tmp;
		return Shooting(out tmp);
	}
	
	void StartDrag(Vector3 start) {
		dragging = true;
		

		dragStart = start;		
		dragPlane = new Plane(new Vector3(0f, 1f, 0f), start);
		
		// draw line
		
		lr.SetPosition (0, start);
		lr.SetPosition (1, start);
		//lr.SetPosition (2, start);
		lr.enabled = true;
		
		
		//Debug.DrawRay(dragStart, new Vector3(0f, 1f, 0f), Color.green, 20f);
		//Debug.Log ("start: " + dragStart);

	}
	
	void EndDrag() {
		dragging = false;

		lr.enabled = false;
	}
}
