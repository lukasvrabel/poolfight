using System;
using System.Collections.Generic;


public class CircularList<T> : List<T>
{
	// current item
	protected int _index;
	
	public CircularList() {
		_index = -1;
	}
	
	// get maximum item from list based on comparator function bool IsGreater(T i1, T i2) (which should be true if i1 > i2)
	public T FindMax(Func<T, T, bool> IsGreater) {
		if(base.Count <= 0) return default(T);
		
		T val = base[0];
		foreach( T item in base.ToArray() ) {
			if(IsGreater(item, val)) {
				val = item;
			}
		}
		return val;
	}
	
	// get maximum item from list based on mapping to float
	public T FindMax(Func<T, float> Numerize) {
		if(base.Count <= 0) return default(T);
		
		T val = base[0];
		foreach( T item in base.ToArray() ) {
			if(Numerize(item) > Numerize(val)) {
				val = item;
			}
		}
		return val;
	}
	
	public new void RemoveAt(int index) {
		base.RemoveAt(index);
		if(index < _index) {
			_Dec();
		}
	}
	
	public new void Remove(T item) {
		int i = base.FindIndex(x => Comparer<T>.Equals(item, x));
		Console.WriteLine("remove() at index: " + i);
		
		if(i <= _index) {
			_index--;
		}
		
		base.Remove(item);
		
	}
	
	public new void RemoveAll(Predicate<T> match) {
		Console.Write("RemoveAll():");
		int i = base.FindIndex(match);
		int toRemove = 0;
		while(i >= 0) {
			//Console.Write(" "+i);
			if(i <= _index) {
				toRemove++;
			}
			i = base.FindIndex(i+1, match);
		}
		//Console.WriteLine();
		_index -= toRemove;
		//for(;toRemove > 0;toRemove--) _Dec();
		
		base.RemoveAll(match);
	}
	
	
	// current item
	public T Current {
		get {return base[_index];}
	}
	
	// current index
	public int CurrentIndex {
		get {return _index;}
	}
	
	// move to next item in circular manner
	public bool MoveNextCircular() {
		if(base.Count <= 0)
			return false;
		_Inc();
		return true;
	}
	
	// move to next until end of list
	public bool MoveNext() {
		if(base.Count <= 0) {
			return false;
		}
		
		_index++;
		if(_index >= base.Count) {
			return false;
		}
		
		return true;
		
	}
	
	// reset the position of index
	public void Reset() {
		_index = -1;
	}
	
	// increase index with rotation
	private void _Inc() {
		_index ++;
		if( _index >= base.Count )
			_index = 0;
	}
	
	// decrease index with rotation
	private void _Dec() {
		_index --;
		if( _index < 0 )
			_index = base.Count-1;
	}
	
	
}

